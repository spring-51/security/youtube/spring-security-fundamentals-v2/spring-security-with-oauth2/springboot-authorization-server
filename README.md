# Authorization Server using spring boot

## Lesson 11:

```lesson

- Code changes pushed WITHOUT NOTES
- clone branch and run spring boot app

- http://127.0.0.1:8080/oauth/token?grant_type=password&username=admin&password=admin&scope=read
  -- pass clientId and secret as http basic auth
- http://127.0.0.1:8080/oauth/token?grant_type=authorization_code&scope=read&code=2NCMqb
  -- pass clientId and secret as http basic auth

```

## Lesson 12:

```lesson

- Code changes pushed WITHOUT NOTES
- clone branch and run spring boot app

- All the above urls from lesson 11
- http://127.0.0.1:8080/oauth/token?grant_type=client_credentials&scope=read
  -- pass clientId and secret as http basic auth
```

## Lesson 13:

```lesson
- authorization_code vs implicit
  -- authorization_code is MORE SECURE then implicit
     as it gets auth code which client uses to get access_token
     while implicit directly gives access_token
  -- in case of more clarity refer lesson 13 of "youtube/spring security fundamentals"
- Code changes pushed WITHOUT NOTES
- clone branch and run spring boot app
```

## lesson 14
```
- grant type : refresh_token
  -- refer - AuthorizationServerConfig -> configure(ClientDetailsServiceConfigurer)
```

## lesson 18
```lesson
keytool command to generate jks file
> keytool -genkeypair -alias myresourceserverkeypair -keyalg RSA  -keypass admin1 -keystore myresourceserverkeypair.jks -storepass admin1

  - this will generate a file called myresourceserverkeypair.jks ,
  is placed in class path i.e ./scr/main/resources/myresourceserverkeypair.jks

  - refer AuthorizationServerConfig -> converter()

  Note:
    - for the sake of example we kept this jks fil under class path.
    In the real world app we shold keep this stored in secure location like Vaults
    and reads from there


```

## lesson 19
```lesson
- here auth server will expose api where resource server will not store public-key to validate token , instead
resource server will call auth server to get public key, then using public key resource server will validate jwt.key

- api
  - GET {bareUrl}/oauth/token_key
  eg. http://127.0.0.1:8080/oauth/token_key

  - by default access to this API is restricted
  - to give access we need to config it
    -- refer - AuthorizationServerConfig -> configure(AuthorizationServerSecurityConfigurer)

Advantages
- when auth server expose api to get public key as per uts private key,
then on rotation of private key resourse server need not to change anything
as it DOES NOT STORE PUBLIC KEY.


```

## after all lesson / Get Token API(login)
```lesson

- here we have added changes for storing jwt in the db
  -- refer TokenController
- for this we whitelisted the TokenController and used rest template to call get token api of same service
  -- refer - UserManagementConfig -> configure(HttpSecurity)
  -- refer - TokenController -> getToken(GetTokenRequest request)


```

## after all lesson / Check Token API(introspection)
```lesson

- here we re-added "refresh_token" grant type to all clients 
  -- refer - MyClientDetails -> getAuthorizedGrantTypes()  

- refresh token endpoint
  -- POST /oauth/token?grant_type=refresh_token&scope=<scope>&refresh_token=<refresh-token> 

- here we have added changes for check token return all claims
  IF TOKEN IS NOT EXPIRED
  
  -- refer TokenController -> checkToken(String token, CheckTokenRequest request)
- for this we whitelisted the TokenController and used rest template to call get token api of same service
  -- refer - UserManagementConfig -> configure(HttpSecurity)
  -- refer - TokenController -> checkToken(String token, CheckTokenRequest request)

  IF TOKEN EXPIRED
  - if token expired we generate refresh token and retrun new token
  -- refer TokenController -> checkToken(String token, CheckTokenRequest request) -> catch block
```

## after all lesson / Expire Token API(logout)
```lesson
- here we have added changes for expire token 
  -- refer TokenController -> expireToken(String token

```

### TODOS

```
1.
Add custom expiry to jwt token
Way 1: added expiry in MyClientDetails
exaample  - ?   
Way 2: added expiry using Token Enhancer
  - refer - MyJwtTokenEnhancer -> enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication)
2.
Add custom claims to jwt token - DONE via using Token Enhancer
  - refer - MyJwtTokenEnhancer -> enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication)
```