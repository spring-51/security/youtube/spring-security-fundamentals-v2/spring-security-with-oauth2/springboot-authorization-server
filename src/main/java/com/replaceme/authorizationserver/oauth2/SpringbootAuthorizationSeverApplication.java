package com.replaceme.authorizationserver.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootAuthorizationSeverApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootAuthorizationSeverApplication.class, args);
	}

}
