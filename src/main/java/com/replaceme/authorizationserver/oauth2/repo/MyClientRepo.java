package com.replaceme.authorizationserver.oauth2.repo;

import com.replaceme.authorizationserver.oauth2.entities.MyClient;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface MyClientRepo extends CrudRepository<MyClient, Long> {

    Optional<MyClient> findClientByClientId(String clientId);
}
