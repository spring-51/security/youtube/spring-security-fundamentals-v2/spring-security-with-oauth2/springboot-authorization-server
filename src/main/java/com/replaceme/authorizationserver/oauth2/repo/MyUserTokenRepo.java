package com.replaceme.authorizationserver.oauth2.repo;

import com.replaceme.authorizationserver.oauth2.entities.MyUserToken;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MyUserTokenRepo extends CrudRepository<MyUserToken, Long> {
    @Query("SELECT mut FROM MyUserToken  mut WHERE mut.expired= false AND mut.jwt =:jwt")
    Optional<MyUserToken> findActiveJwtByJwt(@Param(value = "jwt") String jwt);
}
