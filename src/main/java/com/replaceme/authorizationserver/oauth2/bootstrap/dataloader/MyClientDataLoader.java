package com.replaceme.authorizationserver.oauth2.bootstrap.dataloader;

import com.replaceme.authorizationserver.oauth2.entities.MyClient;
import com.replaceme.authorizationserver.oauth2.repo.MyClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MyClientDataLoader implements CommandLineRunner {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MyClientRepo repo;

    /**
     * Client data loader
     */
    /**
     * Grant Types
     * *****************************************1. password(DEPRECATED)
     * * passes username+pass to client then client passes it to aith server
     * **** IMPORTANT
     * *****************************************2. authorization_code / pkce (MOST AUTH SERVER USES THIS)
     * * we don't pass creds to client when we request client then
     * * client request auth server, then auth server redirects us to its UI
     * * where we pass creds, this way our client does not get any knowledge
     * * of creds
     *
     * *****************************************3. client_credentials
     * * its used for generally health check urls, BEWARE to keep the ACCESS
     * * of the token generated using this grant type be VERY VERY LIMITED
     * *****************************************4. refresh_token
     * * is used in combination with other grant types to get refresh tokens as
     * * one token can be used only once and to access other request we need to have
     * * refresh tokens
     */
    @Override
    public void run(String... args) throws Exception {
        MyClient client1 = new MyClient();
        client1.setClientId("client1");
        client1.setGrantType("password");
        client1.setScope("read");
        client1.setSecret(passwordEncoder.encode("client1"));

        MyClient client2 = new MyClient();
        client2.setClientId("client2");
        client2.setGrantType("authorization_code");
        client2.setReDirectUri("http://127.0.0.1:9000");
        client2.setScope("read");
        client2.setSecret(passwordEncoder.encode("client1"));

        MyClient client3 = new MyClient();
        client3.setClientId("client3");
        client3.setGrantType("client_credentials");
        client3.setScope("read");
        client3.setSecret(passwordEncoder.encode("client1"));

        MyClient client4 = new MyClient();
        client4.setClientId("myresourceserver");
        // client3.setGrantType("client_credentials");
        // client3.setScope("read");
        client4.setSecret(passwordEncoder.encode("client1"));

        Iterable<MyClient> myClientDetails = repo
                .saveAll(
                        Stream.of(client1, client2, client3, client4)
                        .collect(Collectors.toList())
                );
    }
}
