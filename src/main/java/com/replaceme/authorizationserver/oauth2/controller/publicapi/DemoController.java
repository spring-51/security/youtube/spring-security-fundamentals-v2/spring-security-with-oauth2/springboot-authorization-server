package com.replaceme.authorizationserver.oauth2.controller.publicapi;

import com.replaceme.authorizationserver.oauth2.constant.MyConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * We kep this controller here
 * so that we can write some logic here
 * if it's need
 * e.g. if we customize get token or other apis
 *
 * refer - UserManagementConfig -> configure(HttpSecurity http)
 */
@RestController
@RequestMapping(value = MyConstants.DEMO_END_POINT)
public class DemoController {

    @GetMapping
    public String getDemo(){
        return "Hello From DemoController -> getDemo";
    }
}
