package com.replaceme.authorizationserver.oauth2.controller.publicapi.token;

import com.replaceme.authorizationserver.oauth2.constant.MyConstants;
import com.replaceme.authorizationserver.oauth2.dto.CheckTokenRequest;
import com.replaceme.authorizationserver.oauth2.dto.CheckTokenResponse;
import com.replaceme.authorizationserver.oauth2.dto.GetTokenRequest;
import com.replaceme.authorizationserver.oauth2.dto.GetTokenResponse;
import com.replaceme.authorizationserver.oauth2.entities.MyUserToken;
import com.replaceme.authorizationserver.oauth2.repo.MyUserTokenRepo;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = MyConstants.TOKEN_END_POINT)
public class TokenController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private MyUserTokenRepo myUserTokenRepo;

    @PostMapping(value = "/get-token")
    public MyUserToken getToken(@RequestBody GetTokenRequest request){
        String url = String.format(
                "http://127.0.0.1:8080/oauth/token?grant_type=password&scope=read&username=%s&password=%s",
                request.getUsername(),
                request.getUserPassword());
        HttpHeaders headers = createHeadersForGetToken(request.getClientId(), request.getClientSecret());
        HttpEntity<Object> requestEntity = new HttpEntity<>(headers);
        ResponseEntity<GetTokenResponse> mapResponseEntity =
                restTemplate.exchange(url, HttpMethod.POST, requestEntity, GetTokenResponse.class);

        MyUserToken myUserToken = new MyUserToken();
        myUserToken.setUsername(request.getUsername());
        myUserToken.setJwt(mapResponseEntity.getBody().getJwt());
        myUserToken.setRefreshJwt(mapResponseEntity.getBody().getRefreshJwt());
        MyUserToken savedUserToken = myUserTokenRepo.save(myUserToken);

        return myUserTokenRepo.findById(savedUserToken.getId()).orElseThrow(() -> new RuntimeException("User tokent not found"));
    }


    @PostMapping(value = "/check-token")
    public CheckTokenResponse checkToken(@RequestHeader(name = "token") String token, @RequestBody CheckTokenRequest request){
        myUserTokenRepo.findActiveJwtByJwt(token)
                .orElseThrow(() -> new RuntimeException("token is either expired or does not exist"));
        try{
            String url = String.format(
                    "http://127.0.0.1:8080/oauth/check_token?token=%s",
                    token);
        /*
        we are taking clientId and client secret because our auth server has
        isAuthenticated() access config for check token api
        refer - AuthorizationServerConfig
            -> configure(AuthorizationServerSecurityConfigurer)
            -> .checkTokenAccess("isAuthenticated()")
         */
            HttpHeaders headers = createHeadersForGetToken(request.getClientId(), request.getClientSecret());
            HttpEntity<Object> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<CheckTokenResponse> mapResponseEntity =
                    restTemplate.exchange(url, HttpMethod.GET, requestEntity, CheckTokenResponse.class);
            return mapResponseEntity.getBody();
        }catch (Exception e){
            // Assuming this exception is coming because of token expiration
            MyUserToken expiredToken =  myUserTokenRepo.findActiveJwtByJwt(token)
                    .orElseThrow(() -> new RuntimeException("token is either expired or does not exist"));
            expiredToken.setExpired(true);
            String url = String.format(
                    "http://127.0.0.1:8080/oauth/token?grant_type=refresh_token&scope=read&refresh_token=%s",
                    expiredToken.getRefreshJwt());

            HttpHeaders headers = createHeadersForGetToken(request.getClientId(), request.getClientSecret());
            HttpEntity<Object> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<GetTokenResponse> mapResponseEntity =
                    restTemplate.exchange(url, HttpMethod.POST, requestEntity, GetTokenResponse.class);
            GetTokenResponse body = mapResponseEntity.getBody();

            MyUserToken newlyCreatedJwt = new MyUserToken();
            newlyCreatedJwt.setUsername(expiredToken.getUsername());
            newlyCreatedJwt.setJwt(body.getJwt());
            newlyCreatedJwt.setRefreshJwt(body.getRefreshJwt());
            myUserTokenRepo.saveAll(Stream.of(expiredToken, newlyCreatedJwt).collect(Collectors.toList()));

            CheckTokenResponse response = new CheckTokenResponse();
            response.setExpired(Boolean.TRUE);
            /*
            passing new jwt to response as jwt
             */
            response.setRefreshedJwt(body.getJwt());
            return  response;
        }
    }

    @PostMapping(value = "/expire-token")
    public String expireToken(@RequestHeader(name = "token")String token){
        MyUserToken userTokenToExpire =myUserTokenRepo.findActiveJwtByJwt(token)
                .orElseThrow(() -> new RuntimeException("token is either expired or does not exist"));
        userTokenToExpire.setExpired(Boolean.TRUE);
        myUserTokenRepo.save(userTokenToExpire);
        return "Token Expired..!!!!";
    }

    HttpHeaders createHeadersForGetToken(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }
}
