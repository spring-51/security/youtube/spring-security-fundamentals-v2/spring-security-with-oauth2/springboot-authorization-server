package com.replaceme.authorizationserver.oauth2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CheckTokenResponse {
    @JsonProperty(value = "user_name")
    private String username;
    private List<String> scope;
    private Boolean active;
    private String exp;
    private List<String> authorities;
    private String jti;
    @JsonProperty(value = "client_id")
    private String clientId;

    private Boolean expired;
    private String refreshedJwt;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getScope() {
        return scope;
    }

    public void setScope(List<String> scope) {
        this.scope = scope;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public String getRefreshedJwt() {
        return refreshedJwt;
    }

    public void setRefreshedJwt(String refreshedJwt) {
        this.refreshedJwt = refreshedJwt;
    }
}
