package com.replaceme.authorizationserver.oauth2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetTokenResponse {
    @JsonProperty(value = "access_token")
    private String jwt;

    @JsonProperty(value = "refresh_token")
    private String refreshJwt;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getRefreshJwt() {
        return refreshJwt;
    }

    public void setRefreshJwt(String refreshJwt) {
        this.refreshJwt = refreshJwt;
    }
}
