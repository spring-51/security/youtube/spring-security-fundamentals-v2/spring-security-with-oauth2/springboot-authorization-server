package com.replaceme.authorizationserver.oauth2.service;

import com.replaceme.authorizationserver.oauth2.details.MyClientDetails;
import com.replaceme.authorizationserver.oauth2.entities.MyClient;
import com.replaceme.authorizationserver.oauth2.repo.MyClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class MyClientDetailsService implements ClientDetailsService {

    @Autowired
    private MyClientRepo repo;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        MyClientDetails myClientDetails = repo.findClientByClientId(clientId)
                .map(MyClientDetails::new) // same as : .map(e -> new MyClientDetails(e))
                .orElseThrow(() -> new ClientRegistrationException("client not founddd"));
        return myClientDetails;
    }
}
