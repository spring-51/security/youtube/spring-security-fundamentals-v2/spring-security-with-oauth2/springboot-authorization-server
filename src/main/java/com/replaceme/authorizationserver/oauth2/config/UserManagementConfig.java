package com.replaceme.authorizationserver.oauth2.config;

import com.replaceme.authorizationserver.oauth2.constant.MyConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
public class UserManagementConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService uds(){
        InMemoryUserDetailsManager udm = new InMemoryUserDetailsManager();
        /**
         * From lesson 26
         * this user will be able to access end point
         * which has authority of "admin"
         */
        UserDetails adminUser = User.withUsername("admin")
                                .password(passwordEncoder().encode("admin"))
                                .authorities("admin").build();

        udm.createUser(adminUser);

        /**
         * From lesson 26
         * this user will be able to access end point
         * which has authority of "write"
         */
        UserDetails writeUser = User.withUsername("write")
                .password(passwordEncoder().encode("admin"))
                .authorities("write").build();
        udm.createUser(writeUser);

        /**
         * From lesson 26
         * this user will be able to access end point
         * which has authority of "read"
         */
        UserDetails readUser = User.withUsername("read")
                .password(passwordEncoder().encode("admin"))
                .authorities("read").build();
        udm.createUser(readUser);
        return udm;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /* disabling csrf so that we can call
         POST, PUT, PATCH, DELETE
         */

        http.csrf().disable();

        http.authorizeRequests()
                .mvcMatchers(
                        MyConstants.DEMO_END_POINT+"/**",
                        MyConstants.TOKEN_END_POINT+"/**"
                ).permitAll();
        super.configure(http);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


}
