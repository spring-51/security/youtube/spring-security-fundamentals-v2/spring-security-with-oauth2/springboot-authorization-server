package com.replaceme.authorizationserver.oauth2.config;

import com.replaceme.authorizationserver.oauth2.enhancer.MyJwtTokenEnhancer;
import com.replaceme.authorizationserver.oauth2.service.MyClientDetailsService;
import jdk.nashorn.internal.parser.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MyClientDetailsService myClientDetailsService;

    @Value(value = "${myauthserver.tokenstore.type: opaque}")
    private String tokenStoreType;


    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(myClientDetailsService);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        /*
         authenticationManager is created in another
         config(i.e. UserManagementConfig -> authenticationManagerBean())
         */
        /*
        this will help auh server to authenticate users, as authentication manager
        orchestrate user creds authentication in spring security
         */
        endpoints
                .authenticationManager(authenticationManager)
                /*
                the below config required for getting token from refresh token
                POST /oauth/token?grant_type=refresh_token&scope=<scope>&refresh_token=<refresh-toke>
                w/o this we are getting 500 error code
                 */
                /*
                IS THIS A BUG ?
                 */
                .userDetailsService(userDetailsService)

        ;
        // jwt token store - start
        if(tokenStoreType.equalsIgnoreCase("jwt")){

            endpoints
                    .tokenStore(tokenStore())
                    // TODO: QUES : if we already have converter with rokenStore, then why do we need to
                    // specify this again ?
                    .accessTokenConverter(converter());
        }
        // jwt token store - end

        // jwt token enhancer - start
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        /*
        order of enhancer chain is important
        WHY ?
         */
        List<TokenEnhancer> tokenEnhancers = new ArrayList<>();
        tokenEnhancers.add(myJwtTokenEnhancer());
        tokenEnhancers.add(converter());
        tokenEnhancerChain.setTokenEnhancers(tokenEnhancers);
        endpoints.tokenEnhancer(tokenEnhancerChain);
        // jwt token enhancer - end
    }

    // jwt token store - start
    @Bean
    @ConditionalOnProperty(name = "myauthserver.tokenstore.type", havingValue = "jwt")
    public TokenStore tokenStore(){
        TokenStore tokenStore = new JwtTokenStore(converter());
        // config token store - start

        // config token store - end
        return tokenStore;
    }

    @Bean
    @ConditionalOnProperty(name = "myauthserver.tokenstore.type", havingValue = "jwt")
    public JwtAccessTokenConverter converter(){
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        // config Jwt Access Token Converter - start

        // secret config for SYMMETRIC key - start
        // uncommneted in lesson 26 - for dsl resource server - start
        converter.setSigningKey("123456789012345678901234567890123456");
        // secret config for SYMMETRIC key - end

        // secret config for ASYMMETRIC key - start
        // commneted in lesson 26 - for dsl resource server - start
        /*
        KeyStoreKeyFactory keyFactory = new KeyStoreKeyFactory(
                new ClassPathResource("myresourceserverkeypair.jks"),
                // password specified against -keypass in the keytool command to generate the .jks file
                // refer README.md - lesson 18
                "admin1".toCharArray()
        );
         */
        // commneted in lesson 26 - for dsl resource server - end
        /**
         * keyFactory.getKeyPair(..) - will give the secret key - public key pair
         * secret key will be used to generate token
         * the public key will be used by RESOURCE SERVER to validate token
         */
        // commneted in lesson 26 - for dsl resource server - start
        /*
        converter.setKeyPair(
                keyFactory.getKeyPair(
                        // specified against -alias in the keytool command to generate the .jks file
                        // refer README.md - lesson 18
                        "myresourceserverkeypair"
                )
        );
         */
        // commneted in lesson 26 - for dsl resource server - start
        // secret config for ASYMMETRIC key - end

        // config Jwt Access Token Converter - end

        return converter;
    }

    // jwt token store - end

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {

        security
                /*
                this config tells auth server to match client secrets with below encoding
                we can use different password encoders for user password and client secret
                 */
                .passwordEncoder(passwordEncoder)
                /*
                this config tells auth server to allow GET oauth/check_token?token=<token>
                above GET request validates whether access token is valid or not
                 We can use to SpEL
                 1. permitAll() - this will validate token considering  GET oauth/check_token?token=<token>
                 as white list api
                 // .checkTokenAccess("permitAll()")

                 2. isAuthenticated() -  in order to access GET oauth/check_token?token=<token>
                 we need to pass client creds as http basic
                 // .checkTokenAccess("isAuthenticated()")
                 */
                .checkTokenAccess("isAuthenticated()")
                /*
                this config tells auth server to allow GET oauth/token_key
                above GET request return public_key as per private_key of auth server (public,
                private key is govern by jks file of AuthorizationServerConfig -> converter() )
                 We can use to SpEL
                 1. permitAll() - this will return public_key considering  GET oauth/token_key
                 as white list api
                 // .tokenKeyAccess("permitAll()")

                 2. isAuthenticated() -  in order to access GET oauth/token_key
                 we need to pass client creds as http basic
                 // .tokenKeyAccess("isAuthenticated()")
                 */
                .tokenKeyAccess("isAuthenticated()")//
        ;
    }

    @Bean
    public TokenEnhancer myJwtTokenEnhancer(){
        return new MyJwtTokenEnhancer();
    }
}
