package com.replaceme.authorizationserver.oauth2.enhancer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyJwtTokenEnhancer  implements TokenEnhancer {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        DefaultOAuth2AccessToken defaultOAuth2AccessToken = (DefaultOAuth2AccessToken)accessToken;
        /*
        To customize custom expiry
         */
        defaultOAuth2AccessToken.setExpiration(new Date(System.currentTimeMillis() + 11*60*1000));
        String username =  authentication.getName();
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        Map<String, Object> additionalClaims = new HashMap<>();
        additionalClaims.put("claims1","value1");
        additionalClaims.put("usernameeeeeee",userDetails.getUsername());
        additionalClaims.put("isAccountNonExpired",userDetails.isAccountNonExpired()+"");
        defaultOAuth2AccessToken.setAdditionalInformation(additionalClaims);
        return defaultOAuth2AccessToken;
    }
}
