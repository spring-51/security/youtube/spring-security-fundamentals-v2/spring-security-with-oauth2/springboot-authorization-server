package com.replaceme.authorizationserver.oauth2.constant;

public class MyConstants {
    public final static String DEMO_END_POINT = "/demo";
    public final static String TOKEN_END_POINT = "/token";
}
