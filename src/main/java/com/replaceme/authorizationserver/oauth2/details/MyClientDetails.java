package com.replaceme.authorizationserver.oauth2.details;

import com.replaceme.authorizationserver.oauth2.entities.MyClient;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyClientDetails implements ClientDetails {

    private final MyClient myClient;

    public MyClientDetails(MyClient myClient) {
        this.myClient = myClient;
    }

    @Override
    public String getClientId() {
        return this.myClient.getClientId();
    }

    @Override
    public boolean isSecretRequired() {
        return Boolean.TRUE;
    }

    @Override
    public Set<String> getResourceIds() {
        return null;
    }



    @Override
    public String getClientSecret() {
        return this.myClient.getSecret();
    }

    @Override
    public boolean isScoped() {
        return Boolean.TRUE;
    }

    @Override
    public Set<String> getScope() {
        Set<String> scopes = new HashSet<>();
        scopes.add(this.myClient.getScope());
        return scopes;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        Set<String> grantTypes = new HashSet<>();
        grantTypes.add(this.myClient.getGrantType());
        grantTypes.add("refresh_token");
        return grantTypes;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        if(!ObjectUtils.isEmpty(this.myClient.getReDirectUri())){
            return Stream.of(this.myClient.getReDirectUri())
                    .collect(Collectors.toSet());
        }
        return null;
    }

    /**
     * TODO : what's the difference between  GrantedAuthority and scope
     * @return
     */
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(()->this.myClient.getScope());
        return authorities;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return 300;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return 3000;
    }

    // TODO: TBD later
    @Override
    public boolean isAutoApprove(String s) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }
}
